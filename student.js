var Person = require('./person.js');

class Student extends Person {
    constructor(firstname, lastname, nickname, birthyear, studentnumber, email) {
        super(firstname, lastname, nickname, birthyear);
        this.studentnumber = studentnumber;
        this.email = email;
    }
    introduce() {
        super.introduce();
        console.log("My student info is", this.getInfo());
    }
    getInfo() {
        return this.studentnumber + " " + this.email;
    }
 }
var person = new Student("Chumbawamba", "Tubthumper", "Chumb", "1990", "123456", "chumb@hu.hu");

module.exports = Student