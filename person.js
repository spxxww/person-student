// Yhden luokan exporttaus
// tiedostosta person.js
"use strict";

class Person {
  constructor(firstname, lastname, nickname, birthyear) {
    this.firstname = firstname;
    this.lastname = lastname;
    this.nickname = nickname;
    this.birthyear = birthyear;
  }

  introduce() {
    console.log("Hello my name is", this.getName());
  }

  getName() {
    return this.firstname + " " + this.lastname;
  }
}

module.exports = Person;